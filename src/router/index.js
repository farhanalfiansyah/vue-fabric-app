import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import UpdateUser from "../views/UpdateUser.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/update/:id",
    name: "UpdateUser",
    component: UpdateUser,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/fabric",
    name: "Fabric",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Fabric.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
